﻿using UnityEngine;
using System.Collections;

// GENERIC PARENT CLASS
public class Gun : MonoBehaviour {
	
	// Parameters:
	public GameObject AmmoPrefab;
	public float RoundsPerSecond = 1f;
	public float LaunchSpeed = 1f;
	// If the gun aims on its own, how quickly does it turn? Measured in RADIANS PER SECOND.
	public float AimSpeed = 1f;
	// Leave these alone:
	[HideInInspector] public new Transform transform;
	protected float timer=0f;
	protected float period;
	
	
	// CHANGE THIS IN THE CHILD CLASS.
	// OTHERWISE BULLET WILL LAUNCH FROM CENTER OF THE TRANSFORM.
	// DO NOT INCLUDE "base.GetFireLocation();"
	protected virtual Vector3 GetFireLocation()
	{
		return transform.position;
	}
	
	
	// Other scripts will determine which way the gun is pointing.
	// Optional second parameter allows for instant aiming.
	public void AimAt(Vector3 location, bool instant=false)
	{
		if (instant)
			transform.LookAt(location);
		else
		{
			Vector3 look = location - transform.position;
			transform.forward = Vector3.RotateTowards(transform.forward,look, AimSpeed*Time.deltaTime, 0f);
		}
	}
	
	// Maybe other things can force the gun to fire?  Made it public just in case.
	public void Fire()
	{
		timer = 0;
		GameObject bullet = (GameObject)Instantiate(AmmoPrefab, GetFireLocation(), Quaternion.identity);
		Rigidbody rig = bullet.GetComponent<Rigidbody>();
		if (rig)
		{
			rig.velocity = transform.forward * LaunchSpeed;
		}
		else
		{
			Debug.LogError("Disabling " + name + " because its ammo has no rigidbody attached.");
			this.enabled = false;
		}
	}

	// Just in case? This probably shouldn't be used normally.
	public void FireAt(Vector3 targetLocation)
	{
		AimAt(targetLocation, true);
		Fire ();
	}
	
	protected virtual void Awake()
	{
		transform = GetComponent<Transform>();
		if (RoundsPerSecond==0 || AmmoPrefab==null)
			this.enabled = false;
		else
			period = 1f/RoundsPerSecond;
	}
	
	protected virtual void Update()
	{
		if (timer < period)
		{
			timer += Time.deltaTime;
		}
		else
		{
			timer = 0;
			Fire ();
		}
	}
	

}
