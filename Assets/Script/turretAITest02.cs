﻿using UnityEngine;
using System.Collections;
using System;

public class turretAITest02 : MonoBehaviour {
	public GameObject item;
	public Vector3 resultantVector;
	public double desiredAngle;
	public double desiredAngleRads;
	public double newAngley;
	public float speed;
	public bool displayDesiredAngle;
	public bool quadrant3o4;
	public bool quadrant4;
	public double preAngle;
	public double postAngle;
	// Use this for initialization
	void Start () {
		speed = 2;
	}
	
	// Update is called once per frame
	void Update () {
		/*testNum ++;
		Transform bob = GetComponent<Transform>();
		Vector3 temp = new Vector3(testNum,0,0);
		bob.transform.rotation = temp;
		*/
		//item = GameObject.Find ("Capsule");
		resultantVector = (item.transform.position - transform.position);
		desiredAngleRads = Math.Atan (resultantVector.x / resultantVector.z);
		//desiredAngle = ( desiredAngleRads * (180 / Math.PI) );
		//Debug.Log (desiredAngleRads);
		//Debug.Log (desiredAngle);

		if ( (desiredAngleRads * (180 / Math.PI) ) < 0)
		{
			desiredAngle = 360 - Math.Abs(desiredAngleRads * (180 / Math.PI));
		}
		if ( (desiredAngleRads * (180 / Math.PI) ) > 360)
		{
			desiredAngle = (desiredAngleRads * (180 / Math.PI)) - 360;
		}

		preAngle = desiredAngle;
		//desiredAngle = desiredAngle % 360;
		//Debug.Log (desiredAngle);

		if (resultantVector.z < 0)
		{
			quadrant3o4 = true;
			quadrant4 = false;
			desiredAngle = 180 - desiredAngle;
		}
		else if (resultantVector.x < 0)
		{
			quadrant3o4 = false;
			quadrant4 = true;
			desiredAngle = 360 + desiredAngle;
		}
		else
		{
			quadrant4 = false;
			quadrant3o4 = false;
		}

		postAngle = desiredAngle;

		newAngley = transform.rotation.eulerAngles.y;

		if (newAngley < 0)
		{
			newAngley = 360 - Math.Abs(newAngley);
		}
		else if (newAngley > 360)
		{
			newAngley = newAngley - 360;
		}

		while (desiredAngle < 0)
		{
			desiredAngle = 360 - Math.Abs(desiredAngle);
		}
		while (desiredAngle > 360)
		{
			desiredAngle = desiredAngle - 360;
		}

		Vector3 newVel = new Vector3 (0, 1 * speed, 0);
		Vector3 newVel2 = new Vector3 (0, -1 * speed, 0);
		if ((((desiredAngle - newAngley) < 10) && ((desiredAngle - newAngley) > -10)) || (((desiredAngle - newAngley) > 350) && ((desiredAngle - newAngley) < 370)))
		{
		}
		else if (desiredAngle > newAngley)
		{
			transform.Rotate(newVel);
		}
		else if (desiredAngle < newAngley)
		{
			transform.Rotate(newVel2);
		}

		if (displayDesiredAngle){
			Debug.Log(desiredAngle);
		}
	}
}
