﻿using UnityEngine;
using System.Collections;

public class AI_PandorasBox : MonoBehaviour
{
	
	public Transform Target;
	public float Speed = 30.0f;
	public float maxSpeed = 50.0f;
	
	
	void Start ()
	{
		transform.position += transform.forward * Time.deltaTime * Speed;
		
		Speed -= transform.forward.y * Time.deltaTime * 1.0f;
	}
	
	
	void Update ()
	{
		bool Echelon = Input.GetKeyDown(KeyCode.V);
		bool Box = Input.GetKey(KeyCode.B);

		if (Echelon) {

						Target = Target = GameObject.FindGameObjectWithTag ("BoxFrntR").transform;
		
						transform.LookAt (Target);
				}

		if (Box) {

						Target = GameObject.FindGameObjectWithTag ("BoxFrntL").transform;

						transform.LookAt (Target);

				}

	}
}
	
	


