﻿using UnityEngine;
using System.Collections;

public class AI_Catarina : MonoBehaviour
{
	
	public Transform Target;
	public float Speed = 30.0f;
	public float maxSpeed = 50.0f;
	
	
	void Start ()
	{
		transform.position += transform.forward * Time.deltaTime * Speed;
		
		Speed -= transform.forward.y * Time.deltaTime * 1.0f;
	}
	
	
	void Update ()
	{
		if (Input.GetKey (KeyCode.V)) {
			
		Target = GameObject.FindGameObjectWithTag ("EchelonMiddleR").transform;

		transform.LookAt(Target);
		
		transform.position += transform.forward * Time.deltaTime * Speed;
		
		Speed -= transform.forward.y * Time.deltaTime * 1.0f;

			if (Input.GetKey (KeyCode.B)) {
				
				Target = GameObject.FindGameObjectWithTag ("BoxFrntR").transform;
				
				transform.LookAt(Target);
				
				transform.position += transform.forward * Time.deltaTime * Speed;
				
				Speed -= transform.forward.y * Time.deltaTime * 1.0f;
		
		
	}
}
}
}

