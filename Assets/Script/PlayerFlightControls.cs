﻿using UnityEngine;
using System.Collections;

// INTERPRETS PLAYER INPUT TO CONTROL PLAYER'S PLANE
[RequireComponent(typeof(FlightMovement))]
public class PlayerFlightControls : MonoBehaviour {

	private FlightMovement movement;
	public GUIStyle stallStyle;
	public int gear = 1;

	bool giveControl
	{
		get 
		{
			return (!movement.stall);
		}
	}

	void Awake()
	{
		movement = GetComponent<FlightMovement> ();
	}

	void ChangeGear(int g)
	{
		gear = g;
		movement.throttle = 0.1f + gear*0.05f;
		movement.targetSpeed = movement.maxSpeed * 0.1f * gear;
		Debug.Log("Set gear to " + gear);
	}

	void Update()
	{
		// check input
		if (movement.stall) {
			// do nothing
		}
		else if (gear!=1 && Input.GetKey (KeyCode.Alpha1)) {
			ChangeGear(1);
//			gear = 1;
//			movement.throttle = 0.15f;
//			movement.targetSpeed = movement.maximumSpeed * 0.1f;
		}
		else if (gear!=2 && Input.GetKey (KeyCode.Alpha2)) {
			ChangeGear(2);
//			movement.throttle = 0.20f;
//			gear = 2;
//			movement.targetSpeed = movement.maximumSpeed * 0.2f;
		}
		else if (gear!=3 && Input.GetKey (KeyCode.Alpha3)) {
			ChangeGear(3);
//			movement.throttle = 0.25f;
//			gear = 3;
//			movement.targetSpeed = movement.maximumSpeed * 0.3f;
		}
		else if (gear!=4 && Input.GetKey (KeyCode.Alpha4)) {
			ChangeGear(4);
//			movement.throttle = 0.30f;
//			gear = 4;
//			movement.targetSpeed = movement.maximumSpeed * 0.4f;
		}
		else if (gear!=5 && Input.GetKey (KeyCode.Alpha5)) {
			ChangeGear(5);
//			movement.throttle = 0.35f;
//			gear = 5;
//			movement.targetSpeed = movement.maximumSpeed * 0.5f;
		}
		else if (gear!=6 && Input.GetKey (KeyCode.Alpha6)) {
			ChangeGear(6);
//			movement.throttle = 0.40f;
//			gear = 6;
//			movement.targetSpeed = movement.maximumSpeed * 0.6f;
		}
		else if (gear!=7 && Input.GetKey (KeyCode.Alpha7)) {
			ChangeGear(7);
//			movement.throttle = 0.45f;
//			gear = 7;
//			movement.targetSpeed = movement.maximumSpeed * 0.7f;
		}
		else if (gear!=8 && Input.GetKey (KeyCode.Alpha8)) {
			ChangeGear(8);
//			movement.throttle = 0.50f;
//			gear = 8;
//			movement.targetSpeed = movement.maximumSpeed * 0.8f;
		}
		else if (gear!=9 && Input.GetKey (KeyCode.Alpha9)) {
			ChangeGear(9);
//			movement.throttle = 0.55f;
//			gear = 9;
//			movement.targetSpeed = movement.maximumSpeed * 0.9f;
		}
		else if (gear!=10 && Input.GetKey (KeyCode.Alpha0)) {
			ChangeGear(10);
//			movement.throttle = 0.60f;
//			gear = 10;
//			movement.targetSpeed = movement.maximumSpeed;
		}
		movement.axisVertical = Input.GetAxis ("Vertical");
		movement.axisHorizontal = Input.GetAxis ("Horizontal");

		// do stuff
	}

	void OnGUI(){
		if (movement.stall) {
			GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, Screen.width, Screen.height), "STALL", stallStyle);
		}
	}

}
