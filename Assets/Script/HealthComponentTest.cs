using UnityEngine;
using System.Collections;

public class HealthComponentTest: MonoBehaviour 
{
	// These variables are used for the player's health and health bar
	public float curHP=100;
	public float maxHP=100;
	public float maxBAR=100;
	public float maxBARA=100;
	public float curArmor=100;
	public float maxArmor=100;
	public bool armorGone = false;
	public float HealthBarLength;
	public float ArmorBarLength;

	
	void OnGUI()
	{
		// This code creates the health bar at the coordinates 10,10
		GUI.Box(new Rect(10,10,HealthBarLength,25), "");
		// This code determines the length of the health bar
		HealthBarLength=curHP*maxBAR/maxHP;
	}

	void SecondGUI()
	{
		GUI.Box(new Rect(10,4,ArmorBarLength,25), "");
		ArmorBarLength=curArmor*maxBARA/maxArmor;
	}

	
	void ChangeArmor(float Change)
	{
		// This line will take whatever value is passed to this function and add it to curHP.
		curArmor+=Change;
		
		// This if statement ensures that we don't go over the max health
		if(curArmor>maxHP)
		{
			curArmor=100;
		}
		
		// This if statement is to check if the player has died
		if(curArmor<=0)
		{
			// Die
			Debug.Log("Armor Gone!");
			armorGone = true;
		}
	}

	void ChangeHP(float Change)
	{

		curHP+=Change;
		

		if(curHP>maxHP)
		{
			curHP=100;
		}
	}
	

	void OnTriggerEnter(Collider other)
	{
				switch (other.gameObject.tag) {
				case "30Cal":
						ChangeArmor (-1);
						break;
				case "50Cal":
						ChangeArmor (-10);
						break;
				case "20MM":
						ChangeArmor (-25);
						break;
				case "30MM":
						ChangeArmor (-50);
						break;
				case "Blast":
						ChangeArmor (-75);
						break;
				case "Shrapnel":
						ChangeArmor (-5);
						

						if (armorGone == true) {

								switch (gameObject.tag) {
								case "30Cal":
										ChangeHP (-1);
										break;
								case "50Cal":
										ChangeHP (-10);
										break;
								case "20MM":
										ChangeHP (-20);
										break;
								case "30MM":
										ChangeHP (-50);
										break;
													
	
								}

			} break;
				}
		}

	void update (){

		if (curHP == 0)
			
			Destroy(gameObject);
}
}
