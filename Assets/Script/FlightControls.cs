using UnityEngine;
using System.Collections;

public class FlightControls: MonoBehaviour {
	
	//Variables for speed
	public float speed = 0.5f;
	public float maxSpeed = 400.0f;
	//Variables for throttle
	public float throttle = 1.0f;
	public float maxThrottle = 20.0f;
	//Variable for roll speed and pitch speed
	public float rollSpeed = 0.1f;
	public float pitchSpeed = 0.1f;
	//True or false depending on if you want to speed up or not
	public bool speedUp;
	
	public bool state1;
	public bool state2;
	public bool state3;
	public bool state4;
	public bool state5;
	
	public bool planeStart;
	public bool giveControl;
	public bool stall;
	public bool returnFromStall;
	
	public GUIStyle stallStyle;
	
	// Use this for initialization
	void Start () {
		planeStart = true;
		Debug.Log ("Flight script added to: " + gameObject.name);
	}
	
	// Update is called once per frame
	void Update () {
		if (giveControl) {
			if ((Input.GetKey (KeyCode.Alpha1) && planeStart) || (planeStart && returnFromStall)) {
				returnFromStall = false;
				speedUp = true;
				throttle = 0.15f;
				state1 = true;
				state2 = false;
				maxSpeed = 50;
			}
			
			if (Input.GetKey (KeyCode.Alpha2) && (state1 || state3)) {
				speedUp = true;
				throttle = 0.20f;
				state2 = true;
				state1 = false;
				state3 = false;
				planeStart = true;
				maxSpeed = 100;
			}
			
			if (Input.GetKey (KeyCode.Alpha3) && (state2 || state4)) {
				speedUp = true;
				throttle = 0.25f;
				state3 = true;
				state2 = false;
				state4 = false;
				planeStart = false;
				maxSpeed = 150;
			}
			
			if (Input.GetKey (KeyCode.Alpha4) && (state3 || state5)) {
				speedUp = true;
				throttle = 0.30f;
				state4 = true;
				state3 = false;
				state5 = false;
				planeStart = false;
				maxSpeed = 200;
			}
			
			if (Input.GetKey (KeyCode.Alpha5) && state4) {
				speedUp = true;
				throttle = 0.35f;
				state5 = true;
				state4 = false;
				planeStart = false;
				maxSpeed = 250;
			}
			
			if (Input.GetKey (KeyCode.Alpha6)) {
				speedUp = false;
			}
			
			if (speedUp) {
				
				speed += throttle;
				
				transform.position += transform.forward * Time.deltaTime * speed;
				
				speed -= transform.forward.y * Time.deltaTime * 30.0f;
				
				transform.Rotate (pitchSpeed * Input.GetAxis ("Vertical"), 0.0f, rollSpeed * -Input.GetAxis ("Horizontal"));
				
				if (speed > maxSpeed) {
					speed -= 5;
				}
				
//				Debug.Log (Input.GetAxis ("Vertical"));
			}
		} else if (stall) {
			Debug.Log (transform.rotation);
			Debug.Log (transform.rotation.eulerAngles.x);
			if(transform.rotation.eulerAngles.x < 80.0f || transform.rotation.eulerAngles.x > 90.0f){
				if(transform.rotation.eulerAngles.z > 6.0f && transform.rotation.eulerAngles.z < 180.0f){
					transform.Rotate(0.8f, 0.0f, -0.8f);
				}else if(transform.rotation.eulerAngles.z >= 180.0f && transform.rotation.eulerAngles.z < 350.0f){
					transform.Rotate(0.8f, 0.0f, 0.8f);
				}else{
					transform.Rotate(0.8f, 0.0f, 0.0f);
				}
				
				transform.position -= new Vector3(0.0f,1.0f,0.0f);
				
				if(speed < 40.0f){
					speed += 0.15f;
				}
			}else{
				stall = false;
				giveControl = true;
				returnFromStall = true;
				planeStart = true;
				state1 = true;
				state2 = false;
				state3 = false;
				state4 = false;
				state5 = false;
			}
		}
		
	}
	
	private void OnGUI(){
		if (stall) {
			GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, Screen.width, Screen.height), "STALL", stallStyle);
		}
	}
}