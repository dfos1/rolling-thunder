using UnityEngine;
using System.Collections;

public class FlightBehavior : MonoBehaviour {
	//public FlightControls flightControls;
	public GameObject awesomePlane;
	public Camera cam1;
	public Camera cam2;
	
	// Use this for initialization
	void Start () {
		cam1.enabled = true;
		cam2.enabled = false;
		//flightControls = awesomePlane.GetComponent<FlightControls>();
	}
	
	// Update is called once per frame
	void Update () {
		if( awesomePlane.GetComponent<FlightControls>().speed < 0 ){
			Debug.Log ("Plane speed below zero");
			awesomePlane.GetComponent<FlightControls>().giveControl = false;
			awesomePlane.GetComponent<FlightControls>().stall = true;
		}
		
	}
	
	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.name == "Terrain")
		{
			Destroy(awesomePlane);
			cam1.enabled = false;
			cam2.enabled = true;
		}
	}
	
}

