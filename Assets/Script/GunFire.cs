﻿using UnityEngine;
using System.Collections;

public class GunFire : MonoBehaviour {
	
	public Transform Bullet;

	public Transform barrelEnd;

	public bool Firing;
	
	public float Counter = Time.deltaTime;
	
	public float RateOfFire = 0.5f;
	
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (GunRange.inRange == true)
		{
			Firing = true;
		}	
		
		if (GunRange.inRange == false)
		{
			Firing = false;
		}	
		
		if (Firing == true){

			Counter += Time.deltaTime;

			if (RateOfFire < Counter) {

		
		}

	}

}
}
	