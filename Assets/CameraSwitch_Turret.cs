﻿using UnityEngine;
using System.Collections;

public class CameraSwitch_Turret : MonoBehaviour {
	
	public Camera FlightCamera;
	public Camera Nose;
	public Camera Cheek_L;
	public Camera Cheek_R;
	public Camera Dorsal_1;
	public Camera Dorsal_2;
	public Camera Waist_L;
	public Camera Waist_R;
	public Camera Ball;
	public Camera Tail;

	private Camera[] TurretCameras;
	private int currentTurretCameraIndex = 0;
	private Camera currentTurretCamera;
	

	void Start()

				{
						TurretCameras = new Camera[] { Nose, Cheek_L, Cheek_R, Dorsal_1, Dorsal_2, Waist_L, Waist_R, Ball, Tail };//this is the array of cameras
						currentTurretCamera = FlightCamera; //When the program start the main camera is selected as the default camera
						ChangeTurretView ();
				}

	
	// Update is called once per frame

	void Update()
	{
		if(Input.GetKeyDown("t"))
		{
			currentTurretCameraIndex++;
			if (currentTurretCameraIndex > TurretCameras.Length-1)
				currentTurretCameraIndex = 0;
			ChangeTurretView();
		}
	}
	
	void ChangeTurretView()
	{
		currentTurretCamera.enabled = false;
		currentTurretCamera = TurretCameras[currentTurretCameraIndex];
		currentTurretCamera.enabled = true;
	}
}